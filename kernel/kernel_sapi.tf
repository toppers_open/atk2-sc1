$
$  TOPPERS ATK2
$      Toyohashi Open Platform for Embedded Real-Time Systems
$      Automotive Kernel Version 2
$
$  Copyright (C) 2011-2013 by Center for Embedded Computing Systems
$              Graduate School of Information Science, Nagoya Univ., JAPAN
$  Copyright (C) 2011-2013 by FUJISOFT INCORPORATED, JAPAN
$  Copyright (C) 2011-2013 by FUJITSU VLSI LIMITED, JAPAN
$  Copyright (C) 2011-2013 by NEC Communication Systems, Ltd., JAPAN
$  Copyright (C) 2011-2013 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
$  Copyright (C) 2011-2013 by Renesas Electronics Corporation, JAPAN
$  Copyright (C) 2011-2013 by Sunny Giken Inc., JAPAN
$  Copyright (C) 2011-2013 by TOSHIBA CORPORATION, JAPAN
$  Copyright (C) 2011-2013 by Witz Corporation, JAPAN
$
$  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
$  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
$  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
$  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
$      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
$      スコード中に含まれていること．
$  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
$      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
$      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
$      の無保証規定を掲載すること．
$  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
$      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
$      と．
$    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
$        作権表示，この利用条件および下記の無保証規定を掲載すること．
$    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
$        報告すること．
$  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
$      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
$      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
$      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
$      免責すること．
$
$  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
$  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
$  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
$  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
$  の責任を負わない．
$
$  $Id: kernel_sapi.tf 6835 2013-01-23 10:11:15Z fsi-dankei $
$

$ =====================================================================
$ kernel.tfの処理の前に処理されるファイル
$ 静的APIのデータ構造からXMLのデータ構造に変換する処理
$ =====================================================================

$TRACE("EXEC kernel_sapi.tf")$

$ kernel.tfを実行できないようなエラーが発生したか
$FATAL = 0$

$ =====================================================================
$ DEF_OSのエラーチェック
$ =====================================================================

$ 1個定義されているか．
$IF !LENGTH(OS.ID_LIST)$
	$FATAL = 1$
	$ERROR$$FORMAT(_("no DEF_OS is registered"))$$END$
$END$

$ DEF_OS
$ 2個以上定義されていないか．
$IF LENGTH(OS.ID_LIST) > 1$
	$ERROR OS.TEXT_LINE[1]$$FORMAT(_("DEF_OS is duplicated"))$$END$
$END$

$IF EQ(OS.SC[1], "AUTO")$
	$OS.SC[1] = {}$
$END$


$ =====================================================================
$ DEF_HOOKのエラーチェック
$ =====================================================================

$ 1個定義されているか．
$IF !LENGTH(HOOK.ID_LIST)$
	$FATAL = 1$
	$ERROR$$FORMAT(_("no DEF_HOOK is registered"))$$END$
$END$

$ 2個以上定義されていないか．
$IF LENGTH(HOOK.ID_LIST) > 1$
	$ERROR HOOK.TEXT_LINE[1]$$FORMAT(_("DEF_HOOK is duplicated"))$$END$
$END$


$ =====================================================================
$ DEF_OS_STKのエラーチェック
$ =====================================================================

$ 静的API「DEF_OS_STK」が複数ある
$IF LENGTH(OSTK.ID_LIST) > 1$
	$ERROR OSTK.TEXT_LINE[1]$$FORMAT(_("too many %1%"), "DEF_OS_STK")$$END$
$END$


$ =====================================================================
$ DEF_HOOK_STKのエラーチェック
$ =====================================================================

$ 静的API「DEF_HOOK_STK」が複数ある
$IF LENGTH(HSTK.ID_LIST) > 1$
	$ERROR HSTK.TEXT_LINE[1]$$FORMAT(_("too many %1%"), "DEF_HOOK_STK")$$END$
$END$

$ 先頭アドレスがNULLでない場合
$FOREACH hsstkid HSTK.ID_LIST$
	$IF !EQ(HSTK.STK[hsstkid], "NULL")$
		$ERROR HSTK.TEXT_LINE[hsstkid]$$FORMAT(_("STK of DEF_HOOK_STK must be NULL"))$$END$
	$END$
$END$


$ =====================================================================
$ CRE_APPのエラーチェック
$ =====================================================================

$ アプリケーションモードが最低1個は定義されているか．
$IF !LENGTH(APP.ID_LIST)$
	$ERROR$$FORMAT(_("no CRE_APP is registered"))$$END$
$END$


$ =====================================================================
$ CRE_TSKのエラーチェックと関連づけ
$ =====================================================================

$TRACE("ASSOCIATE TASK")$

$FOREACH tskid TSK.ID_LIST$
	$new_list = {}$
	$FOREACH evt TSK.EVT_LIST[tskid]$
		$find = 0$
		$FOREACH evtid EVT.ID_LIST$
			$IF EQ(evt, evtid)$
				$new_list = APPEND(new_list, evtid)$
				$find = 1$
			$END$
		$END$
$		// eventが存在するか．
		$IF !find$
			$ERROR TSK.TEXT_LINE[tskid]$$FORMAT(_("illegal %1% `%2%\'"), "event", evt)$$END$
		$END$
	$END$
	$TSK.EVT_LIST[tskid] = new_list$

	$new_list = {}$
	$FOREACH res TSK.RES_LIST[tskid]$
		$find = 0$
		$FOREACH resid RES.ID_LIST$
			$IF EQ(res, resid)$
				$new_list = APPEND(new_list, resid)$
				$find = 1$
			$END$
		$END$
$		// resourceが存在するか．
		$IF !find$
			$ERROR TSK.TEXT_LINE[tskid]$$FORMAT(_("illegal %1% `%2%\'"), "resource", res)$$END$
		$END$
	$END$
	$TSK.RES_LIST[tskid] = new_list$

	$new_list = {}$
	$FOREACH app TSK.APP_LIST[tskid]$
		$find = 0$
		$FOREACH appid APP.ID_LIST$
			$IF EQ(app, appid)$
				$new_list = APPEND(new_list, appid)$
				$find = 1$
			$END$
		$END$
$		// applicationが存在するか．
		$IF !find$
			$ERROR TSK.TEXT_LINE[tskid]$$FORMAT(_("illegal %1% `%2%\'"), "application", app)$$END$
		$END$
	$END$
	$TSK.APP_LIST[tskid] = new_list$
$END$


$ =====================================================================
$ CRE_RESのエラーチェックと関連づけ
$ =====================================================================

$FOREACH resid RES.ID_LIST$
	$IF EQ(RES.RESATR[resid], "LINKED")$
		$FOREACH resid2 RES.ID_LIST$
			$IF EQ(resid2, RES.LINKEDRESOURCE[resid])$
				$RES.LINKEDRESID[resid] = resid2$
			$END$
		$END$

$		// 被リンク側リソースが存在するか.
		$IF !LENGTH(RES.LINKEDRESID[resid])$
			$ERROR RES.TEXT_LINE[resid]$$FORMAT(_("illegal %1% `%2%\'"), "std_resource", RES.LINKEDRESOURCE[resid])$$END$
			$FATAL = 1$
		$END$
	$END$
$END$


$ =====================================================================
$ CRE_ISRのエラーチェックと関連づけ
$ =====================================================================

$TRACE("ASSOCIATE ISR")$

$FOREACH isrid ISR.ID_LIST$
$	// intatrにENABLEが含まれていない場合
	$IF !(ISR.INTATR[isrid] & ENABLE)$
		$ERROR ISR.TEXT_LINE[isrid]$$FORMAT(_("illegal %1% `%2%\' of `%3%\'"), "intatr", ISR.INTATR[isrid], isrid)$$END$
	$END$

	$new_list = {}$
	$FOREACH res ISR.RES_LIST[isrid]$
		$find = 0$
		$FOREACH resid RES.ID_LIST$
			$IF EQ(res, resid)$
				$new_list = APPEND(new_list, resid)$
				$find = 1$
			$END$
		$END$
$		// resourceが存在するか．
		$IF !find$
			$ERROR ISR.TEXT_LINE[isrid]$$FORMAT(_("illegal %1% `%2%\'"), "resource", res)$$END$
		$END$
	$END$
	$ISR.RES_LIST[isrid] = new_list$
$END$


$ =====================================================================
$ CRE_ALMのエラーチェックと関連付け
$ =====================================================================

$TRACE("ASSOCIATE ALARM")$

$FOREACH almid ALM.ID_LIST$
	$almatr = ALM.ALMATR[almid]$
$	// almatrにACTIVATETASK/SETEVENT/CALLBACK/INCREMENTCOUNTER以外のものが登録されていないか
	$IF !(EQ(almatr, "ACTIVATETASK") || EQ(almatr, "SETEVENT") || EQ(almatr, "CALLBACK") || EQ(almatr, "INCREMENTCOUNTER"))$
		$ERROR ALM.TEXT_LINE[almid]$$FORMAT(_("illegal %1% `%2%\'"), "almatr", almatr)$$END$
	$END$

	$FOREACH cntid CNT.ID_LIST$
$		// ALMとCNTの関連づけ
		$IF EQ(cntid, ALM.COUNTER[almid])$
			$ALM.CNTID[almid] = cntid$
		$END$
	$END$

$	// counterが存在するか
	$IF !LENGTH(ALM.CNTID[almid])$
		$FATAL = 1$
		$ERROR ALM.TEXT_LINE[almid]$$FORMAT(_("illegal %1% `%2%\'"), "counter", ALM.COUNTER[almid])$$END$
	$END$

	$almatr = ALM.ALMATR[almid]$
	$IF EQ(almatr, "ACTIVATETASK") || EQ(almatr, "SETEVENT")$
$		// ALMとTSKの関連づけ
		$FOREACH tskid TSK.ID_LIST$
			$IF EQ(tskid, ALM.TASK[almid])$
				$ALM.TSKID[almid] = tskid$
			$END$
		$END$

$		// taskが存在するか
		$IF !LENGTH(ALM.TSKID[almid])$
			$FATAL = 1$
			$ERROR ALM.TEXT_LINE[almid]$$FORMAT(_("illegal %1% `%2%\'"), "task", ALM.TASK[almid])$$END$
		$END$

		$IF EQ(almatr, "SETEVENT")$
$			// ALMとEVTの関連づけ
			$FOREACH evtid EVT.ID_LIST$
				$IF EQ(evtid, ALM.EVENT[almid])$
					$ALM.EVTID[almid] = evtid$
				$END$
			$END$

$			// eventが存在するか
			$IF !LENGTH(ALM.EVTID[almid])$
				$FATAL = 1$
				$ERROR ALM.TEXT_LINE[almid]$$FORMAT(_("illegal %1% `%2%\'"), "event", ALM.EVENT[almid])$$END$
			$END$
		$END$
	$END$

	$IF EQ(almatr, "INCREMENTCOUNTER")$
		$FOREACH cntid CNT.ID_LIST$
$			// ALMとIncrementCounterの関連づけ
			$IF EQ(cntid, ALM.INCOUNTER[almid])$
				$ALM.INCID[almid] = cntid$
			$END$
		$END$

$		// incounterが存在するか
		$IF !LENGTH(ALM.INCID[almid])$
			$FATAL = 1$
			$ERROR ALM.TEXT_LINE[almid]$$FORMAT(_("nothing %1% `%2%\'"), "incounter", ALM.INCOUNTER[almid])$$END$
		$END$
	$END$

	$new_list = {}$
	$FOREACH app ALM.APP_LIST[almid]$
		$find = 0$
		$FOREACH appid APP.ID_LIST$
			$IF EQ(app, appid)$
				$new_list = APPEND(new_list, appid)$
				$find = 1$
			$END$
		$END$
$		// applicationが存在するか．
		$IF !find$
			$ERROR ALM.TEXT_LINE[almid]$$FORMAT(_("illegal %1% `%2%\'"), "application", app)$$END$
		$END$
	$END$
	$ALM.APP_LIST[almid] = new_list$
$END$


$ =====================================================================
$ CNTのエラーチェックと関連づけ
$ =====================================================================

$TRACE("ASSOCIATE COUNTER")$

$FOREACH cntid CNT.ID_LIST$
	$IF EQ(CNT.CNTATR[cntid], "HARDWARE")$
		$FOREACH isrid ISR.ID_LIST$
			$IF EQ(isrid, CNT.ISR[cntid])$
				$CNT.ISRID[cntid] = isrid$
			$END$
		$END$

		$IF !LENGTH(CNT.ISRID[cntid])$
			$FATAL = 1$
			$ERROR CNT.TEXT_LINE[cntid]$$FORMAT(_("hardware counter %1%'s ISR is not defined in CRE_ISR"), cntid)$$END$
		$END$

	$END$
$END$


$ =====================================================================
$ CRE_SCHTBLのエラーチェックと関連付け
$ =====================================================================

$TRACE("ASSOCIATE SCHEDULETABLE")$

$FOREACH schtblid SCHTBL.ID_LIST$
$	// SCHTBLとCNTの関連づけ
	$FOREACH cntid CNT.ID_LIST$
		$IF EQ(cntid, SCHTBL.COUNTER[schtblid])$
			$SCHTBL.CNTID[schtblid] = cntid$
		$END$
	$END$

$	// 駆動カウンタは存在するか
	$IF !LENGTH(SCHTBL.CNTID[schtblid])$
		$FATAL = 1$
		$ERROR SCHTBL.TEXT_LINE[schtblid]$$FORMAT(_("schedule table `%1%\' shall be driven by one counter"), schtblid)$$END$
	$END$

	$new_list = {}$
	$FOREACH app SCHTBL.APP_LIST[schtblid]$
		$find = 0$
		$FOREACH appid APP.ID_LIST$
			$IF EQ(app, appid)$
				$new_list = APPEND(new_list, appid)$
				$find = 1$
			$END$
		$END$
$		// applicationが存在するか．
		$IF !find$
			$ERROR SCHTBL.TEXT_LINE[schtblid]$$FORMAT(_("illegal %1% `%2%\'"), "application", app)$$END$
		$END$
	$END$
	$SCHTBL.APP_LIST[schtblid] = new_list$
$END$


$ =====================================================================
$ DEF_EXPIREPTのエラーチェックと関連付け
$ =====================================================================

$FOREACH expptactid EXPPTACT.ID_LIST$
	$IF !(EQ(EXPPTACT.EXPIREATR[expptactid], "ACTIVATETASK") || EQ(EXPPTACT.EXPIREATR[expptactid], "SETEVENT"))$
		$FATAL = 1$
		$ERROR EXPPTACT.TEXT_LINE[expptactid]$$FORMAT(_("illegal %1% `%2%\' in %3%"), "expireatr", EXPPTACT.EXPIREATR[expptactid], "DEF_EXPIREPT")$$END$
	$END$

$	// SCHTBLとEXPPTACTの関連づけ
	$FOREACH schtblid SCHTBL.ID_LIST$
		$IF EQ(EXPPTACT.SCHEDULETABLE[expptactid], schtblid)$
			$EXPPTACT.SCHTBLID[expptactid] = schtblid$
		$END$
	$END$

	$schtblid = EXPPTACT.SCHTBLID[expptactid]$

	$IF !LENGTH(schtblid)$
$		// スケジュールテーブルに属さないアクション
		$ERROR EXPPTACT.TEXT_LINE[expptactid]$$FORMAT(_("scheduletable(%1%) is not defined"), EXPPTACT.SCHEDULETABLE[expptactid])$$END$
	$END$

$	// EXPPTACTとTSKとの関連づけ
	$FOREACH tskid TSK.ID_LIST$
		$IF EQ(tskid, EXPPTACT.TASK[expptactid])$
			$EXPPTACT.TSKID[expptactid] = tskid$
		$END$
	$END$

$	// taskが存在するか
	$IF !LENGTH(EXPPTACT.TSKID[expptactid])$
		$FATAL = 1$
		$ERROR EXPPTACT.TEXT_LINE[expptactid]$$FORMAT(_("illegal %1% `%2%\' in %3%"), "task", EXPPTACT.TASK[expptactid], "DEF_EXPIREPT")$$END$
	$END$

	$IF EQ(EXPPTACT.EXPIREATR[expptactid], "SETEVENT")$
$		// EXPPTACTとEVTとの関連づけ
		$FOREACH evtid EVT.ID_LIST$
			$IF EQ(evtid, EXPPTACT.EVENT[expptactid])$
				$EXPPTACT.EVTID[expptactid] = evtid$
			$END$
		$END$

$		// eventが存在するか
		$IF !LENGTH(EXPPTACT.EVTID[expptactid])$
			$FATAL = 1$
			$ERROR EXPPTACT.TEXT_LINE[expptactid]$$FORMAT(_("illegal %1% `%2%\' in %3%"), "event", EXPPTACT.EVENT[expptactid], "DEF_EXPIREPT")$$END$
		$END$
	$END$
$END$

$	// SCHTBLにEXPPTACTが一つは存在するか
$FOREACH schtblid SCHTBL.ID_LIST$
	$find = 0$
	$FOREACH expptactid EXPPTACT.ID_LIST$
		$IF EQ(EXPPTACT.SCHTBLID[expptactid], schtblid)$
			$find = 1$
		$END$
	$END$

	$IF !find$
		$FATAL = 1$
		$ERROR SCHTBL.TEXT_LINE[schtblid]$$FORMAT(_("DEF_EXPIREPT for scheduletable(%1%) does not exist"), schtblid)$$END$
	$END$
$END$

$IF FATAL$
	$DIE()$
$END$
